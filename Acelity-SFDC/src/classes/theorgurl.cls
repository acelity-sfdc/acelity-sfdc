public with sharing class theorgurl {

public boolean showMessage {get;set;}
    public PageReference RedirectPage() {
    
  integer verOffset=0;
  integer nameOffset=0;
  string browserName='Microsoft Internet Explorer' ;
  String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
  string nAgt= userAgent ;
  system.debug('-------------------userAgent -----------'+userAgent );
   // In Opera, the true version is after "Opera" or after "Version"
if ((verOffset=nAgt.indexOf('Opera'))!=-1) {
 browserName = 'Opera';
 
}
// In MSIE, the true version is after "MSIE" in userAgent
else if ((verOffset=nAgt.indexOf('MSIE'))!=-1) {
 browserName = 'Microsoft Internet Explorer';
 
}
// In Chrome, the true version is after "Chrome" 
else if ((verOffset=nAgt.indexOf('Chrome'))!=-1) {
 browserName = 'Chrome';
 
}
// In Safari, the true version is after "Safari" or after "Version" 
else if ((verOffset=nAgt.indexOf('Safari'))!=-1) {
 browserName = 'Safari';
 
}
// In Firefox, the true version is after "Firefox" 
else if ((verOffset=nAgt.indexOf('Firefox'))!=-1) {
 browserName = 'Firefox';
 
}
// In most other browsers, "name/version" is at the end of userAgent 
else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
          (verOffset=nAgt.lastIndexOf('/')) ) 
{
 browserName = nAgt.substring(nameOffset,verOffset);
 if (browserName.toLowerCase()==browserName.toUpperCase()) {
  //browserName = navigator.appName;
 }
}

system.debug('=================baseURL ========'+baseURL );
system.debug('========================browserName ======================'+browserName );
If(browserName=='Microsoft Internet Explorer')
{
        showMessage =true;
        system.debug('=================baseURL ==22======'+baseURL+'/secur/logout.jsp');
        PageReference pageRef = new PageReference(baseURL+'/secur/logout.jsp');
        pageRef.setRedirect(true);

     }

        return null;
    }

public static string baseURL {get;set;}
    

public theorgurl()
{
  baseURL = URL.getSalesforceBaseUrl().toExternalForm();
  showMessage =false;

}
    @AuraEnabled
    public static String fetchit(){
   string urler =URL.getSalesforceBaseUrl().toExternalForm();
   baseURL =urler;
     return (urler);
    }
    
@AuraEnabled
public static String getBaseUrl () {
   
    return URL.getSalesforceBaseUrl().toExternalForm();
}
    @AuraEnabled
public static String getUIThemeDescription() {
    return UserInfo.getUiThemeDisplayed();
}
}